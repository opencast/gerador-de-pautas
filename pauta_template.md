---
title: "{{pauta}}"
author: "{{convidado}}"
geometry: "left=1cm,right=1cm,top=2cm,bottom=2cm"
output: pdf_document
---

# Pauta Opencast 

## Titulo: {{pauta}}
## Convidado: {{convidado}}

# Apresentação
 - Avisar das redes sociais
 - Avisar que temos os canais dos hosts e do convidado

# Desenvolvimento
 - Editar no gitlab
{% for pergunta in perguntas %}
 - {{ pergunta |trim}}
{% endfor %}

# Conclusão 
 - Momento cultural
 - Abrir pra perguntas na live e encerrar o podcast