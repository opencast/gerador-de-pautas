import os 
from slugify import slugify
import jinja2
import pypandoc

templateLoader = jinja2.FileSystemLoader(searchpath="./")
templateEnv = jinja2.Environment(loader=templateLoader)
TEMPLATE_FILE = "pauta_template.md"
template = templateEnv.get_template(TEMPLATE_FILE)

with open('pautas.txt', 'r') as f:
    lines = f.readlines()
    perguntas = list();
    for line in lines:
        if (line[0]== '?'):
            perguntas.append(line[1::])
        else:
            pauta = line[3::].split(':')
            folder = slugify(pauta[0].title()).replace("-","_")

            if not os.path.isdir('./pautas'):
                os.mkdir("pautas")

            if not os.path.isdir(os.path.join("pautas", folder)):
                os.mkdir(os.path.join("pautas", folder))

            with open(os.path.join(os.path.join("pautas", folder), f'{slugify(pauta[0].title())}.md'), 'w') as pauta_md:
                outputText = template.render(pauta = pauta[0].title(), convidado = pauta[1].title(), perguntas = perguntas) 
                pauta_md.write(outputText)
            pypandoc.convert_file(os.path.join(os.path.join("pautas", folder), f'{slugify(pauta[0].title())}.md'), 'pdf', outputfile=os.path.join(os.path.join("pautas", folder), f'{slugify(pauta[0].title())}.pdf'))
            perguntas = list()